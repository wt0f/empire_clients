FROM alpine:3 as build

LABEL MAINTAINER='Gerard Hickey <hickey@kinetic-compute.com>'

ADD Empire-0.122/ /tmp/Empire/
ADD pei2/ /tmp/pei2/
ADD eif-1.3.4 /tmp/eif/

RUN apk update && apk add perl perl-dev musl-dev make gcc ncurses-dev readline-dev
RUN perl -MCPAN -e "install 'CPAN::DistnameInfo'" && \
    perl -MCPAN -e "install 'Term::ReadKey'" && \
    perl -MCPAN -e "install 'Lingua::EN::Numericalize'" && \
    perl -MCPAN -e "install 'Time::ParseDate'"
RUN cd /tmp/Empire && perl Makefile.PL && make install
RUN cd /tmp/pei2 && LIBS=-lcurses make
# RUN cd /tmp/eif && CFLAGS=-O2 LIBS=-lposix ./configure && make

# Build the production container with just the assets
FROM alpine:3 as package

COPY --from=build /usr/local/share/perl5 /usr/local/share/perl5/
COPY --from=build /usr/local/lib/perl5 /usr/local/lib/perl5/
COPY --from=build /tmp/pei2/highlight /usr/local/bin/
COPY --from=build /tmp/pei2/pei /usr/local/bin/
COPY --from=build /tmp/pei2/*.pl /usr/local/lib/perl5/site_perl/
COPY --from=build /tmp/pei2/help* /usr/local/lib/perl5/site_perl/

ADD entrypoint.sh /usr/local/bin/
ADD pei3/pei3 /usr/local/bin/
ADD Support/termcap /etc/termcap
ADD pei3/example.pei3rc /usr/local/share/example.pei3rc
ADD pei2/example.peirc /usr/local/share/example.peirc

WORKDIR /empire
RUN chmod 755 /usr/local/bin/pei /usr/local/bin/pei3 && \
    adduser -h /empire -g 'Empire User' -D empire && \
    chown empire:empire /empire && chmod 755 /empire && \
    apk --no-cache add perl ncurses readline

USER empire

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
CMD ["/bin/sh"]