#!/bin/sh

[ ! -f /empire/.peirc ] && cp /usr/local/share/example.peirc /empire/.peirc
[ ! -f /empire/.pei3rc ] && cp /usr/local/share/example.pei3rc /empire/.pei3rc

exec "$@"
